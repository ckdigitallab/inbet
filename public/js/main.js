;$(() => {
    let $menuBtn = $('._menuToggle'),
        $menu = $('._menu'),
        $userMenuBtn = $('._userMenuToggle'),
        $userMenu = $('._userMenu'),
        $modal = $('._filterDropdown'),
        $modalCloseBtn = $('._closeFilterDropdown'),
        $modalOpenBtn = $('._openModal');



    const openMenu = () => {
        closeUserMenu();
        $menu.addClass('active');
        $menuBtn.addClass('active')
    };
    const closeMenu = () => {
        $menu.removeClass('active');
        $menuBtn.removeClass('active')
    };
    const toggleMenu = () => {
        if($menu.hasClass('active')){
            closeMenu();
        }else{
            openMenu();
        }
    };

    const openUserMenu = () => {
        closeMenu();
        $userMenuBtn.addClass('active');
        $userMenu.addClass('active');
    };
    const closeUserMenu = () => {
        $userMenuBtn.removeClass('active');
        $userMenu.removeClass('active');
    };
    const toggleUserMenu = () => {
        if($userMenu.hasClass('active')){
            closeUserMenu();
        }else{
            openUserMenu();
        }
    };

    let isModalOpen = false;
    const openModal = (e) => {
        e.preventDefault();
        $modal.addClass('active');
        isModalOpen = true;
    };
    const closeModal = (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('close');
        $modal.removeClass('active');
        isModalOpen = false;
    };
    let offset = 0;
    const disableScroll = () => {
        offset = window.pageYOffset;
        $('body, html').addClass('overflow');
        $('body').css({top: -offset});
    };
    const enableScroll = () => {
        $('body, html').removeClass('overflow');
        $('body').css({top: 0});
        window.scrollTo(0, offset);
    };




    $('._owlBanner').addClass('owl-carousel').owlCarousel({items: 1, loop: true, nav: true, navText: ['', '']});
    $('._newsOwl').addClass('owl-carousel').owlCarousel({items: 1, loop: true, nav: true, navText: ['', '']});
    $('._giftsOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, loop: true, nav: true, navText: ['', '']},
            768: {items: 2, loop: false, nav: false, mouseDrag: false, touchDrag: false},
            1260: {items: 4, loop: false}
        }
    });
    $('._benefitOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, loop: true},
            550: {items: 3, loop: false}
        }
    });

    let $newsListOwl = $('._newsListOwl');
    $newsListOwl.addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, loop: true, nav: false, navText: ['', '']},
            768: {items: 2, loop: true},
            1260: {items: 4, loop: true}
        }
    });

    $('._newsNext').click(function() {
        $newsListOwl.trigger('next.owl.carousel');
    });
    $('._newsPrev').click(function() {
        $newsListOwl.trigger('prev.owl.carousel');
    });

    $modalOpenBtn.on('click', openModal);
    $modalCloseBtn.on('click', closeModal);
    $modal.on('click', (e) => { e.stopPropagation(); });
    $menuBtn.on('click', toggleMenu);
    $userMenuBtn.on('click', toggleUserMenu);

    $('._authToggle').on('click', () => {
        $('.header').toggleClass('auth');
        $('._userMenu').removeClass('active');
    });

    class Select{
        constructor($el){
            this.$el = $el;
            this.options = {};
            this.$el.find('option').each((i, el) => {
                let $el = $(el);
                this.options[$el.attr('value')] = $el.text();
            });

            $el.wrap('<div class="select"></div>');
            this.$root = $el.parent();

            let selected = $el.find('option:selected').val(),
                selectedText = $el.find('option:selected').text();
            this.$root.append('<div class="select__btn">'+selectedText+'</div>');
            this.$btn = this.$root.find('.select__btn');

            this.$root.append('<div class="select__drop"><div class="select__content"></div></div>');

            this.$list = this.$root.find('.select__content');

            Object.keys(this.options).forEach(val => {
                this.$list.append('<div class="select__item '+(val === selected ? 'selected' : '')+'" data-val="'+val+'">'+this.options[val]+'</div>');
            });

            this.state = {
                open: false
            };
            this.initEvents();
        }
        close(){
            this.state.open = false;
            this.$root.removeClass('open');
        }
        open(){
            this.state.open = true;
            this.$root.addClass('open');
        }
        toggle(){
            if(this.state.open){
                this.close();
            }else{
                this.open();
            }
        }
        select(val){
            this.$list.find('.select__item').removeClass('selected');
            let $selected = this.$list.find('.select__item[data-val="'+val+'"]')
            $selected.addClass('selected');
            this.$btn.text($selected.text());
        }
        initEvents(){
            let _this = this;
            this.$root.on('click', (e) => { e.stopPropagation(); });
            $('body').on('click', () => { this.close(); });
            this.$btn.on('click', () => { this.toggle(); });
            this.$root.find('.select__item').on('click', function () {
                let $el = $(this),
                    val = $el.attr('data-val');

                _this.select(val);
                _this.close();
            })
        }
    };

    $('._select').each((i, el) => {
        new Select($(el));
    });
});

function chg_type() {
    // Получить имеющееся поле ввода
    var old_field=document.getElementById('pass');
    // Создать новое поле ввода
    var new_field=document.createElement('input');
    new_field.name='pass';
    new_field.id='pass';
    new_field.className='input';
    // Переключение типа поля "текст"<->"пароль"
    new_field.type=(old_field.type=='text')?'password':'text';
    // Сохранить уже введенный текст
    new_field.value=old_field.value;
    // Заменить имеющееся поле ввода новым
    document.getElementById('passplace').replaceChild(new_field,old_field);
    // Поменять картинку
    var img_field=document.getElementById('pass_img');
    img_field.src=(old_field.type=='text')?'img/Iconhide.png':'img/Icon.png';
}

function showBonus() {
    $(".register").hide();
    $(".gifts").show();
}